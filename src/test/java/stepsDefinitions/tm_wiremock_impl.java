package stepsDefinitions;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

import org.testng.Assert;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.MappingBuilder;
import com.github.tomakehurst.wiremock.matching.StringValuePattern;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class tm_wiremock_impl {

	

	
@Given("^A wiremock server is started for trackingFavorites service\\.$")
	public void a_wiremock_server_is_started() throws Throwable {
		
//		this.wireMockServer = new WireMockServer(options());
				// .dynamicPort()
				// .bindAddress("127.1.1.1")
				//this.wireMockServer.start();
	
	}

@Then("^Make a GET request for trackingFavorites and validate the response\\.$")
	public void make_a_GET_request_for_trackingFavorites_and_validate_the_response() throws Throwable {

		// Creating a Stub - Canned Response (Static Response)
		stubFor(get(urlEqualTo("/api/next/graphql/?operationName=TrackingFavorites")).willReturn(aResponse().withHeader("Content-Type", "application/json")
				.withBody("{\n" + "    \"data\": {\n" + "        \"trackingFavorites\": {\n"
						+ "            \"items\": [\n" + "                {\n"
						+ "                    \"Date\": 1571739217,\n"
						+ "                    \"Entity_Id\": \"K8vZ9171oMf\",\n"
						+ "                    \"Entity_Id_Source\": \"DISCOVERY\",\n"
						+ "                    \"Entity_Id_Type\": \"ATTRACTION\",\n"
						+ "                    \"Favorite_Id\": \"0e54117b-7659-41ea-9a27-ff10f28227f8\",\n"
						+ "                    \"Legacy_Id\": \"805955\",\n"
						+ "                    \"__typename\": \"Tracking_Favorite\"\n" + "                },\n"
						+ "                {\n" + "                    \"Date\": 1571739218,\n"
						+ "                    \"Entity_Id\": \"K8vZ9171oM7\",\n"
						+ "                    \"Entity_Id_Source\": \"DISCOVERY\",\n"
						+ "                    \"Entity_Id_Type\": \"ATTRACTION\",\n"
						+ "                    \"Favorite_Id\": \"c606961c-a929-41b5-8db6-9e3d82dd7dd0\",\n"
						+ "                    \"Legacy_Id\": \"805973\",\n"
						+ "                    \"__typename\": \"Tracking_Favorite\"\n" + "                },\n"
						+ "                {\n" + "                    \"Date\": 1571739226,\n"
						+ "                    \"Entity_Id\": \"KovZ917Ah1H\",\n"
						+ "                    \"Entity_Id_Source\": \"DISCOVERY\",\n"
						+ "                    \"Entity_Id_Type\": \"VENUE\",\n"
						+ "                    \"Favorite_Id\": \"febec2f4-2779-4182-b856-68afa8ba232c\",\n"
						+ "                    \"Legacy_Id\": \"230012\",\n"
						+ "                    \"__typename\": \"Tracking_Favorite\"\n" + "                },\n"
						+ "                {\n" + "                    \"Date\": 1571741173,\n"
						+ "                    \"Entity_Id\": \"K8vZ9171oV7\",\n"
						+ "                    \"Entity_Id_Source\": \"DISCOVERY\",\n"
						+ "                    \"Entity_Id_Type\": \"ATTRACTION\",\n"
						+ "                    \"Favorite_Id\": \"dc92d5d8-12b4-4bb2-8eb1-cc556b19edf8\",\n"
						+ "                    \"Legacy_Id\": \"805920\",\n"
						+ "                    \"__typename\": \"Tracking_Favorite\"\n" + "                },\n"
						+ "                {\n" + "                    \"Date\": 1571897314,\n"
						+ "                    \"Entity_Id\": \"K8vZ9171oM7\",\n"
						+ "                    \"Entity_Id_Source\": \"DISCOVERY\",\n"
						+ "                    \"Entity_Id_Type\": \"ATTRACTION\",\n"
						+ "                    \"Favorite_Id\": \"81d3243d-befd-4ff0-b32f-b1b30b7c4df8\",\n"
						+ "                    \"Legacy_Id\": \"806157\",\n"
						+ "                    \"__typename\": \"Tracking_Favorite\"\n" + "                },\n"
						+ "                {\n" + "                    \"Date\": 1571898058,\n"
						+ "                    \"Entity_Id\": \"K8vZ9171ouV\",\n"
						+ "                    \"Entity_Id_Source\": \"DISCOVERY\",\n"
						+ "                    \"Entity_Id_Type\": \"ATTRACTION\",\n"
						+ "                    \"Favorite_Id\": \"7bb3fd12-606a-4404-b588-23c7bf5513a8\",\n"
						+ "                    \"Legacy_Id\": \"806157\",\n"
						+ "                    \"__typename\": \"Tracking_Favorite\"\n" + "                },\n"
						+ "                {\n" + "                    \"Date\": 1571899665,\n"
						+ "                    \"Entity_Id\": \"Entity_Id\",\n"
						+ "                    \"Entity_Id_Source\": \"DISCOVERY\",\n"
						+ "                    \"Entity_Id_Type\": \"ATTRACTION\",\n"
						+ "                    \"Favorite_Id\": \"9782d973-17d6-41f1-8b21-960cff30c605\",\n"
						+ "                    \"Legacy_Id\": \"806157\",\n"
						+ "                    \"__typename\": \"Tracking_Favorite\"\n" + "                },\n"
						+ "                {\n" + "                    \"Date\": 1571900689,\n"
						+ "                    \"Entity_Id\": \"{{Entity_Id}}\",\n"
						+ "                    \"Entity_Id_Source\": \"DISCOVERY\",\n"
						+ "                    \"Entity_Id_Type\": \"ATTRACTION\",\n"
						+ "                    \"Favorite_Id\": \"8343a21e-7c4e-4801-a212-8d2c585be730\",\n"
						+ "                    \"Legacy_Id\": \"806157\",\n"
						+ "                    \"__typename\": \"Tracking_Favorite\"\n" + "                },\n"
						+ "                {\n" + "                    \"Date\": 1571901032,\n"
						+ "                    \"Entity_Id\": \"K8vZ9175T17\",\n"
						+ "                    \"Entity_Id_Source\": \"DISCOVERY\",\n"
						+ "                    \"Entity_Id_Type\": \"ATTRACTION\",\n"
						+ "                    \"Favorite_Id\": \"ea1aa176-8809-4884-b0d7-f8db96ec085a\",\n"
						+ "                    \"Legacy_Id\": \"806157\",\n"
						+ "                    \"__typename\": \"Tracking_Favorite\"\n" + "                },\n"
						+ "                {\n" + "                    \"Date\": 1571901034,\n"
						+ "                    \"Entity_Id\": \"K8vZ9174Za7\",\n"
						+ "                    \"Entity_Id_Source\": \"DISCOVERY\",\n"
						+ "                    \"Entity_Id_Type\": \"ATTRACTION\",\n"
						+ "                    \"Favorite_Id\": \"82548e3b-64c3-402d-9f45-c3531e01ab3d\",\n"
						+ "                    \"Legacy_Id\": \"806157\",\n"
						+ "                    \"__typename\": \"Tracking_Favorite\"\n" + "                },\n"
						+ "                {\n" + "                    \"Date\": 1571901035,\n"
						+ "                    \"Entity_Id\": \"K8vZ9171oV7\",\n"
						+ "                    \"Entity_Id_Source\": \"DISCOVERY\",\n"
						+ "                    \"Entity_Id_Type\": \"ATTRACTION\",\n"
						+ "                    \"Favorite_Id\": \"28befdf0-68eb-4a52-a568-674a8273d4f5\",\n"
						+ "                    \"Legacy_Id\": \"806157\",\n"
						+ "                    \"__typename\": \"Tracking_Favorite\"\n" + "                },\n"
						+ "                {\n" + "                    \"Date\": 1572932374,\n"
						+ "                    \"Entity_Id\": \"K8vZ9173kl0\",\n"
						+ "                    \"Entity_Id_Source\": \"DISCOVERY\",\n"
						+ "                    \"Entity_Id_Type\": \"ATTRACTION\",\n"
						+ "                    \"Favorite_Id\": \"28cc3739-d74b-492a-bfab-ec5231645704\",\n"
						+ "                    \"Legacy_Id\": \"2317395\",\n"
						+ "                    \"__typename\": \"Tracking_Favorite\"\n" + "                },\n"
						+ "                {\n" + "                    \"Date\": 1572932375,\n"
						+ "                    \"Entity_Id\": \"K8vZ9171Jo7\",\n"
						+ "                    \"Entity_Id_Source\": \"DISCOVERY\",\n"
						+ "                    \"Entity_Id_Type\": \"ATTRACTION\",\n"
						+ "                    \"Favorite_Id\": \"5a871892-0f24-4c06-b962-8ce4f0ce447b\",\n"
						+ "                    \"Legacy_Id\": \"2317395\",\n"
						+ "                    \"__typename\": \"Tracking_Favorite\"\n" + "                }\n"
						+ "            ],\n" + "            \"__typename\": \"Tracking_PaginatedResponse\"\n"
						+ "        }\n" + "    }\n" + "}")));

		
		//Creating a request - Mocking the wireMock server 
		RestAssured.baseURI = "http://34.93.124.68:9000";
		RequestSpecification httpRequest = RestAssured.given();
		System.out.println("URL is: " + RestAssured.baseURI);
		
		
		//Fetching the response....
		Response response = httpRequest.get("/api/next/graphql/?operationName=TrackingFavorites");
		System.out.println("Response is: ...." + response.getBody().asString());
		
		System.out.println("Response is: ...." + response.statusCode());
		
		//Sample assertions
		Assert.assertEquals(200, response.statusCode());
		System.out.println("Assertion passed for status code, code is = " + response.getStatusCode() + "\n" );


	
	}


@Then("^shutdown the wiremock server for trackingFavorites service\\.$")
	public void shutdown_the_wiremock_server() throws Throwable {

		//this.wireMockServer.stop();
	
	}
	
	
	
	
@Given("^A wiremock server is started for addFavorite service\\.$")
	public void a_wiremock_server_is_started_for_addFavorite() throws Throwable {
	
	//	this.wireMockServer = new WireMockServer(options());
		// .dynamicPort()
		// .bindAddress("127.1.1.1")
		//this.wireMockServer.start();

		
	}

	@Then("^Make a POST request for addFavorites and validate the response\\.$")
	public void make_a_POST_request_for_addFavorites_and_validate_the_response() throws Throwable {
	
	System.out.println("**************** Executing addFavorites API tests *******************");
		
		// Creating a Stub - Canned Response (Static Response)
		MappingBuilder builder = post(urlEqualTo("/api/next/graphql"));
	    StringValuePattern body = equalToJson("{\"operationName\":\"OnboardingPushFavorite\",\"variables\":{\"input\":{\"Entity_Id\":\"K8vZ917KVr7\",\"Device_Type\":\"DESKTOP\",\"Device_Id\":null,\"Entity_Id_Source\":\"DISCOVERY\",\"Entity_Id_Type\":\"ATTRACTION\",\"Type\":\"FAV\",\"Source\":\"TMAPP\",\"Customer_Id\":\"eaOJga4y6-GhHsCKu8uzpQ\",\"Hmac_Id\":\"278c610f9f8a3b5e2b0eaa6a05a6d1c59bbcfbfec2892618be6417e5ff212645\",\"Legacy_Id\":\"2317395\"}},\"extensions\":{\"persistedQuery\":{\"version\":1,\"sha256Hash\":\"bba28e2776cfe34932008b68547b5390257a0f1cc2332ebabdfe0867c89979e6\"}}}", false, false);
	    stubFor(builder.withRequestBody(body)
	    		.willReturn(aResponse().withHeader("Content-Type", "application/json")
				.withBody("{\n" + 
						"    \"data\": {\n" + 
						"        \"trackingFavoritePush\": {\n" + 
						"            \"Date\": 1573117651,\n" + 
						"            \"Entity_Id\": \"K8vZ917KVr7\",\n" + 
						"            \"Entity_Id_Source\": \"DISCOVERY\",\n" + 
						"            \"Entity_Id_Type\": \"ATTRACTION\",\n" + 
						"            \"Legacy_Id\": \"2317395\",\n" + 
						"            \"Favorite_Id\": \"6bf0b60d-65af-4d23-a9c6-f3b731529a68\",\n" + 
						"            \"Source\": \"TMAPP\",\n" + 
						"            \"__typename\": \"Tracking_Favorite\"\n" + 
						"        }\n" + 
						"    }\n" + 
						"}")));

		
		//Creating a request - Mocking the wiremock server
		RestAssured.baseURI = "http://34.93.124.68:9000";
		RequestSpecification httpRequest = RestAssured.given()
				.body("{\n" + 
						"	\"operationName\": \"OnboardingPushFavorite\",\n" + 
						"	\"variables\": {\n" + 
						"		\"input\": {\n" + 
						"			\"Entity_Id\": \"K8vZ917KVr7\",\n" + 
						"			\"Device_Type\": \"DESKTOP\",\n" + 
						"			\"Device_Id\": null,\n" + 
						"			\"Entity_Id_Source\": \"DISCOVERY\",\n" + 
						"			\"Entity_Id_Type\": \"ATTRACTION\",\n" + 
						"			\"Type\": \"FAV\",\n" + 
						"			\"Source\": \"TMAPP\",\n" + 
						"			\"Customer_Id\": \"eaOJga4y6-GhHsCKu8uzpQ\",\n" + 
						"			\"Hmac_Id\": \"278c610f9f8a3b5e2b0eaa6a05a6d1c59bbcfbfec2892618be6417e5ff212645\",\n" + 
						"			\"Legacy_Id\": \"2317395\"\n" + 
						"		}\n" + 
						"	},\n" + 
						"	\"extensions\": {\n" + 
						"		\"persistedQuery\": {\n" + 
						"			\"version\": 1,\n" + 
						"			\"sha256Hash\": \"bba28e2776cfe34932008b68547b5390257a0f1cc2332ebabdfe0867c89979e6\"\n" + 
						"		}\n" + 
						"	}\n" + 
						"}");

		//Fetching the response....
		Response response = httpRequest.post("/api/next/graphql");
				
				
		System.out.println("Response is: ...." + response.getBody().asString());
		
		//Sample assertions
		Assert.assertEquals(200, response.statusCode());
		
		System.out.println("Response is: ...." + response.statusCode());
		
		Assert.assertEquals(response.body().jsonPath().getString("data.trackingFavoritePush.Entity_Id"), "K8vZ917KVr7");
		System.out.println("Assertion passed for status code = " + response.getStatusCode());
		System.out.println("Assertion passed for Entity_id, It is = " + response.body().jsonPath().getString("data.trackingFavoritePush.Entity_Id"));

	
	}

	@Then("^shutdown the wiremock server for addFavorite service\\.$")
	public void shutdown_the_wiremock_server_for_addFavorite() throws Throwable {
	
		
	}
	
	@Given("^A wiremock server is started for removeFavorite service\\.$")
	public void a_wiremock_server_is_started_for_removeFavorite() throws Throwable {

		
	}

	@Then("^Make a POST request for removeFavorites and validate the response\\.$")
	public void make_a_POST_request_for_removeFavorites_and_validate_the_response() throws Throwable {
	    
		System.out.println("**************** Executing removeFavorites API tests ***************** \n");

		// Creating a Stub - Canned Response (Static Response)
		MappingBuilder builder = post(urlEqualTo("/api/next/graphql"));
		StringValuePattern body = equalToJson(
				"{\"operationName\":\"OnboardingRemoveFavorite\",\"variables\":{\"favoriteId\":\"K8vZ9171fF7\"},\"extensions\":{\"persistedQuery\":{\"version\":1,\"sha256Hash\":\"f9c5f557312200eca8f764ff217ebc80c06f28afe64765fdd9f1e614bb7e3445\"}}}",
				false, false);
		stubFor(builder.withRequestBody(body).willReturn(aResponse()
				.withHeader("Content-Type", "application/json")
				.withBody("{\n" + "    \"data\": {\n" + "        \"trackingFavoriteRemove\": {\n"
						+ "            \"Favorite_Id\": \"K8vZ9171fF7\",\n"
						+ "            \"__typename\": \"Tracking_Favorite\"\n" + "        }\n" + "    }\n" + "}")));

		// Creating a request - Mocking the wireMock server
		//RestAssured.baseURI = this.wireMockServer.baseUrl();
		RestAssured.baseURI = "http://34.93.124.68:9000";
		//RestAssured.baseURI = "http://localhost:8080";
		RequestSpecification httpRequest = RestAssured.given().body("{\n" + 
				"	\"operationName\": \"OnboardingRemoveFavorite\",\n" + 
				"	\"variables\": {\n" + 
				"		\"favoriteId\": \"K8vZ9171fF7\"\n" + 
				"	},\n" + 
				"	\"extensions\": {\n" + 
				"		\"persistedQuery\": {\n" + 
				"			\"version\": 1,\n" + 
				"			\"sha256Hash\": \"f9c5f557312200eca8f764ff217ebc80c06f28afe64765fdd9f1e614bb7e3445\"\n" + 
				"		}\n" + 
				"	}\n" + 
				"}");

		// Fetching the response....
		Response response = httpRequest.post("/api/next/graphql");

		System.out.println("Response is: ...." + response.getBody().asString());

		// Sample assertions
		Assert.assertEquals(200, response.statusCode());

		System.out.println("Response is: ...." + response.statusCode());
		
		
		Assert.assertEquals(response.body().jsonPath().getString("data.trackingFavoriteRemove.Favorite_Id"), "K8vZ9171fF7");
		System.out.println("Assertion passed for status code = " + response.getStatusCode());
		System.out.println("Assertion passed for Entity_id, It is = "
				+ response.body().jsonPath().getString("data.trackingFavoriteRemove.Favorite_Id"));

		
	}

	@Then("^shutdown the wiremock server for removeFavorites service\\.$")
	public void shutdown_the_wiremock_server_for_removeFavorite() throws Throwable {
	    
	}

	
}


/*
 
 Mappings - 
 Below is the curl command to push/save the mapping files. [ Stubs required for testing ]
 
$ curl -X POST \
--data '{ "request" : {
      "url" : "/api/next/graphql",
      "method" : "POST",
      "bodyPatterns" : [ {
        "equalToJson" : "{\"operationName\":\"OnboardingRemoveFavorite\",\"variables\":{\"favoriteId\":\"K8vZ9171fF7\"},\"extensions\":{\"persistedQuery\":{\"version\":1,\"sha256Hash\":\"f9c5f557312200eca8f764ff217ebc80c06f28afe64765fdd9f1e614bb7e3445\"}}}",
        "ignoreArrayOrder" : false,
        "ignoreExtraElements" : false
      } ]
    },
    "response" : {
      "status" : 200,
      "body" : "{\n    \"data\": {\n        \"trackingFavoriteRemove\": {\n            \"Favorite_Id\": \"K8vZ9171fF7\",\n            \"__typename\": \"Tracking_Favorite\"\n        }\n    }\n}",
      "headers" : {
        "Content-Type" : "application/json"
      }
    }}' \
http://34.93.124.68:9000/__admin/mappings/new
 
 
 * */
