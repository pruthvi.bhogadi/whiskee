#   This script is to start the docker containers for
# - UI testing (Firefox and Chrome browsers connected via Selenium Grid)
# - Wiremock server for mocking APIs

# Prerequisite to run this script ===> Make sure you have DOCKER installed.

# Running this script ====> sh startDockerContainers.sh

echo " "
echo "Cleaning up old selenium-grid and removing dead containers"
echo "=========================================================="
echo " "
yes | docker network prune
docker rm $(docker ps -a -q)
echo " "
echo "Creating Selenium-Grid and connecting FF and Chrome browsers to Grid"
echo "===================================================================="
echo " "
docker network create grid
docker run -d -p 4444:4444 --net grid --name selenium-hub selenium/hub
docker run -d --net grid -e HUB_HOST=selenium-hub -v /dev/shm:/dev/shm seleni
um/node-chrome
docker run -d --net grid -e HUB_HOST=selenium-hub -v /dev/shm:/dev/shm seleni
um/node-firefox
echo " "
echo "Creating WireMock Servers"
echo "==========================="
echo " "
docker run -d --rm -p 9000:8080 rodolpheche/wiremock
docker run -d --rm -p 9001:8443 rodolpheche/wiremock --https-port 8443
echo " "
echo ".................All Containers Deployed.................."
echo "=========================================================="
echo " "